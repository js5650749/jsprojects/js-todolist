let toDoList = [];
let keyword = "";
while (keyword !== "quit") {
    keyword = prompt("What would you like to do? new - add a todo | list - list todos | delete - delete a todo | quit - quit the app");
    switch (keyword.toLowerCase()) {
        case "new":
            toDoList.push(prompt("What would be the todo?"));
            break;
        case "list":
            console.log("********************");
            for (let idx=0; idx<toDoList.length; idx++) {
                console.log(`${idx} - ${toDoList[idx]}`);
            }
            break;
        case "delete":
            let userChoice = prompt("Which element do you want to delete?");
            let index = toDoList.indexOf(userChoice);
            toDoList.splice(index,1);
            break;
        case "quit":
            console.log("Ok, you quit!")
            break;
        default:
            console.log("You gave an invalid parameter. Please, choose from the options!");
    }
}






